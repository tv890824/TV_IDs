import re
import json
import gitlab
import subprocess
import requests
import time
import tempfile
import os

def is_ipfs_running():
    try:
        response = requests.get("http://127.0.0.1:5001/webui")
        # Si la respuesta tiene código 200, significa que el daemon está activo
        return response.status_code == 200
    except requests.exceptions.RequestException:
        # Si hay algún error (como la conexión rechazada), retorna False
        return False


def sobrescribir_historial(gitlab_token, archivos_contenido):
    # Crear un directorio temporal
    with tempfile.TemporaryDirectory() as temp_dir:
        # Clonar el repositorio con profundidad mínima
        repo_url = f'https://oauth2:{gitlab_token}@gitlab.com/tv890824/TV_IDs.git'
        subprocess.run(['git', 'clone', '--depth', '1', repo_url, temp_dir], check=True)

        # Cambiar al directorio del repositorio clonado
        os.chdir(temp_dir)

        # Actualizar los archivos
        for file_path, content in archivos_contenido.items():
            with open(file_path, 'w') as f:
                f.write(content)

        # Realizar un commit único sobrescribiendo el historial
        subprocess.run(['git', 'add', '.'], check=True)
        subprocess.run(['git', 'commit', '--amend', '-m', 'Sobrescribir historial: Actualización de archivos'],
                       check=True)

        # Push forzado para reemplazar el historial en el repositorio remoto
        subprocess.run(['git', 'push', '--force'], check=True)


def main():

    pathIPFSexe = '/usr/local/bin/ipfs'

    try:
        daemon_process = subprocess.Popen([pathIPFSexe, "daemon"])
    except FileNotFoundError:
        print(f"Error: No se encontró el ejecutable de IPFS en {pathIPFSexe}. Verifica la ruta.")
        return
    except:
        print(f"Error al iniciar el daemon de IPFS")
        return

    timeout = 60  # Segundos máximos para esperar
    start_time = time.time()
    resp = is_ipfs_running()
    while not is_ipfs_running():
        if time.time() - start_time > timeout:
            print("Error: El daemon de IPFS no se activó en el tiempo esperado.")
            return
        time.sleep(1)
    print("Daemon de IPFS está activo.")

    url = 'https://ipfs.io/ipns/k51qzi5uqu5dgg9al11vomikugim0o1i3l3fxp3ym3jwaswmy9uz8pq4brg1u9'
    try:
        # Intentamos hacer la solicitud GET
        response = requests.get(url)

        # Verificamos si la solicitud fue exitosa (código 200)
        response.raise_for_status()  # Esto lanzará una excepción para códigos de error HTTP

        # Si bien, obtenemos el contenido HTML
        html_content = response.text

    except requests.exceptions.RequestException as e:
        # Si ocurre algún error (conexión, HTTP, etc.), se captura aquí
        print(f"Hubo un error al intentar acceder a la URL: {e}")
        return

    # Expresión regular para encontrar el contenido del atributo "links"
    match = re.search(r'"links":\s*(\[[^\]]*\])', html_content)
    if match:
        # Extraer el JSON como string
        links_json = match.group(1)
        # Convertir el JSON a lista de diccionarios
        enlaces_acestream = json.loads(links_json)
    else:
        print("No se encontró el atributo 'links'.")

    # Lista para almacenar las IDs de acestream
    ids_acestream = []
    # Lista para almacenar los nombres de acestream
    nombres_acestream = []
    # Itera sobre los enlaces y extrae la ID y el nombre
    for enlace in enlaces_acestream:
        url_acestream = enlace['url']
        nombre_acestream = enlace['name']
        try:
            id_acestream = url_acestream.split('acestream://')[1]
        except:
            id_acestream = ''

        ids_acestream.append(id_acestream)
        nombres_acestream.append(nombre_acestream)

    m3uGeneric = ""
    # Agregar la línea inicial del archivo
    m3uGeneric += "#EXTM3U\n"
    # Generar las líneas para cada pareja ID-Nombre
    for id_acestream, nombre_acestream in zip(ids_acestream, nombres_acestream):
        linea_extinf = f'#EXTINF:-1 tvg-name="{nombre_acestream}" group-title="amateur" group-title="{nombre_acestream}", {nombre_acestream}\n'
        linea_url = f'http://127.0.0.1:6878/ace/getstream?id={id_acestream}&hlc=1&transcode_audio=0&transcode_mp3=0&transcode_ac3=0&preferred_audio_language=eng\n'
        m3uGeneric += linea_extinf
        m3uGeneric += linea_url

    m3uGabriel = m3uGeneric

    with open("/home/asiotegi/Documentos/gitlab_token.txt") as file:
        gitlab_token = file.read().strip()

    # Configurar la conexión con GitLab
    gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlab_token)
    # Obtener el proyecto
    project = gl.projects.get('tv890824/TV_IDs')

    # Contenido de los archivos a actualizar
    archivos_contenido = {
        'listas_canales_GENERIC.m3u': m3uGeneric,
        'listas_canales_GABRIEL.m3u': m3uGabriel
    }

    sobrescribir_historial(gitlab_token, archivos_contenido)
    print("Archivos actualizados y historial sobrescrito con éxito.")

    print("Cerrando el daemon de IPFS...")
    daemon_process.terminate()  # Enviar señal SIGTERM
    daemon_process.wait()       # Esperar a que termine
    print("Daemon cerrado.")


if __name__ == '__main__':
    main()

